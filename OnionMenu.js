(function OnionMenu(angular, undefined) {
    'use strict';

    var app = angular.module('app');

    app.service('OnionMenu', function OnionMenu($rootScope, $log, OnionMenuRing, inputService) {
        var OnionMenu = function OnionMenu(center_x, center_y, elements, startRing) {
            var self = this;
            this.center_x = center_x;
            this.center_y = center_y;

            this.elements = elements;

            this.rings = [];

            var radius = 50;

            this.rings[0] = this.ringify(elements[startRing], 1);
            // this.rings[1] = this.ringify(elements['two'], 2);
            // this.rings[2] = this.ringify(elements['three'], 3);
            // this.rings[3] = this.ringify(elements['four'], 4);

            $rootScope.$on('showRing', function(event, ring, shell){

              // $log.log(ring,shell);
              if(ring && shell) {
                // $log.log('showing ring ', ring, 'at', shell);
                // $log.log(self.ringify(self.elements[ring], shell));

                self.rings[shell-1] = self.ringify(self.elements[ring], shell);
                // for(var i = shell - 1; i < self.rings.length - 1; i++) {
                //   if(self.rings.hasOwnProperty(i)){
                //     $log.log('removing shell', shell);
                //     // delete self.rings[i];
                //   }
                // }
                $log.log('removing ', shell);
                self.rings.splice(shell, 1);
                $log.log(self.rings.length);
              }
            });



            // $log.log('MENU',this.menu);

            // this.drawTextAlongArc(ctx, 'Defend', x, y, 170, -spacer*4, forth-spacer*2);



        };

        OnionMenu.prototype.ringify = function(obj, shell) {
          var ring = new OnionMenuRing({
              shell: shell,
              elementThickness: 50,
              radius: 60 * shell,
              elements: obj.elements,
              center_x: this.center_x,
              center_y: this.center_y
          });

          return ring;
        }

        OnionMenu.prototype.update = function update() {

          for (var i = 0; i < this.rings.length; i++) {
            this.rings[i].update();
          }

          // for(var n = 0; n < this.menu.elements.length; n++) {
          //   var element = this.menu.elements[n];
          //   if(element.selected) {
          //     // $log.log(element);
          //     this.rings.push(new OnionMenuRing({
          //         elementThickness: 50,
          //         radius: 50 + 10,
          //         elements: [{name:'asd'}],
          //         center_x: this.center_x,
          //         center_y: this.center_y
          //     })
          //     );
          //   }
          // }
        }

        OnionMenu.prototype.draw = function draw(ctx) {

            ctx.save();
            ctx.translate(this.center_x, this.center_y);

            this.drawCenter(ctx, this.center_x, this.center_y, 50);

            for (var i = 0; i < this.rings.length; i++) {
              this.rings[i].draw(ctx);
            }

            ctx.restore();

        }

        OnionMenu.prototype.drawCenter = function mouseMove(ctx, x, y, radius) {

            ctx.save();
            ctx.beginPath();
            ctx.fillStyle='#afafaf';
            ctx.arc(0, 0, 30, 0, Math.PI * 2);
            ctx.fill();
            ctx.restore();

        }

        return OnionMenu;

    });
}(window.angular));
