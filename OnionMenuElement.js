(function OnionMenuElement(angular, undefined) {
  'use strict';

  var app = angular.module('app');

  app.service('OnionMenuElement', function OnionMenuElement($rootScope, $log, inputService) {
    var OnionMenuElement = function(options) {
      this.options = options;

      this.options.color = +Math.floor(Math.random()*16777215).toString(16);

      this.startAngle = this.options.id * this.options.width + this.options.id * this.options.spacerWidth;
      this.endAngle = (this.options.id + 1) * this.options.width + this.options.id * this.options.spacerWidth;
      this.selected = true;
    }

    OnionMenuElement.prototype.update = function update() {
      // if (this.inRing(ƒolor = '#00cc00';
      // } else if (this.isInsideSector()) {
      //   this.options.color = '#cc0000';
      // } else if (this.inRing()) {
      //   this.options.color = '#0000ff';
      // } else {
      //   this.options.color = '#afafaf';
      // }


      if (this.inRing() && this.isInsideSector()) {
        // $log.log('adding new ring', this.options.shell, this.options.triggersRing)
        if(this.options.triggersRing){
          $rootScope.$broadcast('showRing', this.options.triggersRing, this.options.shell + 1);
        }
        this.options.color = '#cc0000';
        this.selected = true;
      } else {
        this.options.color = '#afafaf';
        this.selected = false;
      }
    };

    OnionMenuElement.prototype.draw = function draw(ctx) {
      ctx.beginPath();
      // $log.log(this.options.color);
      ctx.strokeStyle = this.options.color;
      ctx.arc(0, 0, this.options.radius, this.startAngle, this.endAngle);
      $log.log(this.options);
      ctx.stroke();

    };

    OnionMenuElement.prototype.inRing = function inRing() {
      var center_x = this.options.center_x,
        center_y = this.options.center_y,
        y = inputService.getX(),
        x = inputService.getY(),
        r = parseInt(this.options.radius) + parseInt(this.options.elementThickness) / 2 - 2,
        r2 = parseInt(this.options.radius) - parseInt(this.options.elementThickness) / 2 + 2;

      return this.inCircle(center_x, center_y, r, x, y) && !this.inCircle(center_x, center_y, r2, x, y);
    };

    OnionMenuElement.prototype.inCircle = function(center_x, center_y, radius, x, y) {
      var square_dist = Math.pow(center_x - x, 2) + Math.pow(center_y - y, 2);
      return square_dist <= Math.pow(radius, 2);
    }

    OnionMenuElement.prototype.isInsideSector = function() {
      var center_x = this.options.center_x,
        center_y = this.options.center_y,
        xx = inputService.getX(),
        yy = inputService.getY();

      // $log.log(this.startAngle, this.endAngle);

      var deltaX = center_x - yy;
      var deltaY = center_y - xx;
      var theta = -Math.atan2(deltaX, deltaY); // In radians
      // theta is now in the range -Math.PI to Math.PI
      if (theta < 0) {
        theta = 2*Math.PI + theta;
      }

      return this.startAngle < theta && theta < this.endAngle;
    };


    OnionMenuElement.prototype.drawTextAlongArc = function drawTextAlongArc(ctx, str, centerX, centerY, radius, angle, width, padding) {
        ctx.save();
        ctx.rotate(Math.PI / 2);
        ctx.font = '12pt Calibri';
        ctx.textAlign = 'center';
        ctx.fillStyle = 'blue';
        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 4;

        var len = str.length, s;
        ctx.translate(centerX, centerY);
        ctx.rotate(- (angle + padding));
        for(var n = 0; n < len; n++) {
            ctx.rotate((width / len) - padding);
            ctx.save();
            ctx.translate(0, -1 * radius);
            s = str[n];
            ctx.fillText(s, 0, 0);
            ctx.restore();
        }
        ctx.restore();
    };

    return OnionMenuElement;

  });
}(window.angular));
