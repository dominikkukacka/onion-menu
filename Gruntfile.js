'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT });
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    // configurable paths
    var yeomanConfig = {
        app: '.'
    };

    grunt.initConfig({
        yeoman: yeomanConfig,
        bower: {
            install: {
                options: {
                    targetDir: 'bower',
                    copy: false,
                    install: true,
                    verbose: true
                }
            }
        },
        watch: {
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: [
                    '*.{html,js}'
                ]
            }
        },
        connect: {
            options: {
                port: 9000,
                hostname: '0.0.0.0'
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, yeomanConfig.app)
                        ];
                    }
                }
            }
        },
        open: {
            server: {
                url: 'http://localhost:<%= connect.options.port %>'
            }
        }
    });

    grunt.registerTask('install', [
        'install-dependencies',
        'bower:install'
    ]);

    grunt.registerTask('server', [
        'connect:livereload',
        'open',
        'watch'
    ]);
    grunt.registerTask('default', [
        'server'
    ]);
};