var app = angular.module('app', []);//'angular-memory-stats'

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

app.directive('onionMenu', function onionMenuDirective(OnionMenu, inputService, $log) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var ctx = element[0].getContext('2d');

            var center_x = element[0].width / 2,
                center_y = element[0].height / 2;

            element[0].onmousemove = inputService.mouseMove;

            var rings = {
              'start': {
                label: 'start',
                elements: [
                  {
                    label: 'open two',
                    triggersRing: 'two'
                  },
                  {
                    label: 'open three',
                    triggersRing: 'three'
                  },
                  {
                    label: 'open four',
                    triggersRing: 'four'
                  }
                ]
              },
              'two': {
                label: 'two',
                elements: [
                  {
                    label: 'two one',
                    triggersRing: 'three'
                  },
                  {
                    label: 'two two',
                    triggersRing: 'four'
                  }
                ]
              },
              'three': {
                label: 'three',
                elements: [
                  {
                    label: 'three one',
                  },
                  {
                    label: 'three two',
                  },
                  {
                    label: 'three three',
                  }
                ]
              },
              'four': {
                label: 'four',
                elements: [
                  {
                    label: 'four one',
                  },
                  {
                    label: 'four two',
                  },
                  {
                    label: 'four three',
                  },
                  {
                    label: 'four four',
                  }
                ]
              },
              'five': {
                label: 'five',
                elements: [
                  {
                    label: 'five one',
                  },
                  {
                    label: 'five two',
                  },
                  {
                    label: 'five three',
                  },
                  {
                    label: 'five four',
                  },
                  {
                    label: 'five five',
                  }
                ]
              }
            }

            var onionMenu = new OnionMenu(center_x, center_y, rings, 'start');

            function update() {
                onionMenu.update();
            }

            function render() {
                ctx.clearRect (0, 0, element[0].width, element[0].height);

                onionMenu.draw(ctx);
            }

            (function animloop(){
                requestAnimFrame(animloop);
                update();
                render();
            })();

        }
    };
});


app.service('inputService', function($log) {
    var x = null;
    var y = null;

    this.mouseMove = function(e) {
        x = e.y;
        y = e.x;
    }

    this.getX = function getX() {
        return x;
    }

    this.getY = function getY() {
        return y;
    }
});
