(function OnionMenuRing(angular, undefined) {
    'use strict';

    var app = angular.module('app');

    app.service('OnionMenuRing', function OnionMenuRing($log, OnionMenuElement, inputService) {
        var OnionMenuRing = function(options) {
            this.options = options;

            var spacerWidth = Math.PI * 2 / 180;
            var elementWidth = Math.PI * 2 / this.options.elements.length - spacerWidth;

            this.elements = [];
            for (var i = 0; i < this.options.elements.length; i++) {
                var _element = this.options.elements[i];

                var element = new OnionMenuElement({
                    label: _element.label,
                    radius: this.options.radius,
                    width: elementWidth,
                    spacerWidth: spacerWidth,
                    triggersRing: _element.triggersRing,
                    shell: this.options.shell,
                    id: i,
                    center_x: this.options.center_x,
                    center_y: this.options.center_y,
                    elementThickness: this.options.elementThickness
                });
                this.elements.push(element);
            };
            // $log.log(this.elements);
      };

        OnionMenuRing.prototype.update = function() {
            for(var n = 0; n < this.elements.length; n++) {
                this.elements[n].update();
            }
        };

        OnionMenuRing.prototype.draw = function(ctx) {
            ctx.save();
            ctx.rotate(Math.PI / 2 + Math.PI);

            ctx.lineWidth = this.options.elementThickness;

            // ctx.rotate(spacer/2);
            for(var n = 0; n < this.elements.length; n++) {
                this.elements[n].draw(ctx);
            }

            ctx.restore();
        };



        return OnionMenuRing;

    });
}(window.angular));
